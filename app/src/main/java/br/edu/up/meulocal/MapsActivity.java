package br.edu.up.meulocal;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity
    implements OnMapReadyCallback, LocationListener,
    GoogleApiClient.ConnectionCallbacks {

  private GoogleMap mMap;
  private GoogleApiClient gClient;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_maps);

    //Responsável por notificar o fragmento para carregar o mapa.
    SupportMapFragment smf = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
    smf.getMapAsync(this);


    //Cliente responsável por conectar o LocationServices.
    gClient = new GoogleApiClient.Builder(this)
        .addApi(LocationServices.API)
        .addConnectionCallbacks(this)
        .build();

    gClient.connect();
  }

  /**
   * Método de retorno (CallBack) chamado, de forma assíncrona, quando
   * o mapa está pronto para uso. Este método é gerado na implementação
   * da interface OnMapReadyCallback.
   */
  @Override
  @SuppressWarnings({"MissingPermission"})
  public void onMapReady(GoogleMap googleMap) {

    mMap = googleMap;
    mMap.setMyLocationEnabled(true);
    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    mMap.getUiSettings().setCompassEnabled(true);
    mMap.getUiSettings().setZoomControlsEnabled(true);
    mMap.getUiSettings().setMapToolbarEnabled(true);

    //Coordenadas da Universidade Positivo;
    LatLng up = new LatLng(-25.446828, -49.358875);
    atualizarMapa(up);
  }

  private void atualizarMapa(LatLng latLng) {

    //Carregamento do ícone;
    Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.up_pin);

    //Criação do marcador;
    MarkerOptions marcador = new MarkerOptions();
    marcador.icon(BitmapDescriptorFactory.fromBitmap(bmp));
    marcador.position(latLng);
    marcador.title("UP");
    mMap.addMarker(marcador);

    //Atualização da localização;
    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng,16);
    mMap.moveCamera(update);
  }

  /**
   * Método de retorno (CallBack) chamado, de forma assíncrona, quando
   * o GoogleApiClient conecta no LocationServices. Este método é
   * gerado na implementação da interface ConnectionCallbacks.
   */
  @Override
  @SuppressWarnings({"MissingPermission"})
  public void onConnected(@Nullable Bundle bundle) {

    LocationRequest request = new LocationRequest();
    request.setInterval(1000);
    LocationServices.FusedLocationApi.requestLocationUpdates(gClient, request, this);
  }

  /**
   * Método de retorno (CallBack) chamado, de forma assíncrona, quando
   * o GoogleApiClient desconeta do LocationServices. Este método é
   * gerado na implementação da interface ConnectionCallbacks.
   */
  @Override
  public void onConnectionSuspended(int i) {
    LocationServices.FusedLocationApi.removeLocationUpdates(gClient, this);
  }

  /**
   * Método de retorno (CallBack) chamado, de forma assíncrona,
   * quando a localização do dispositivo é alterada. Este método é
   * gerado na implementação da interface LocationListener.
   */
  @Override
  @SuppressWarnings({"MissingPermission"})
  public void onLocationChanged(Location location) {

    double latitude = location.getLatitude();
    double longitude = location.getLongitude();

    LatLng latLng = new LatLng(latitude, longitude);
    atualizarMapa(latLng);
  }
}